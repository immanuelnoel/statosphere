var mostDayCommits = 0;
var mostMonthCommits = 0;
var yearStats = [{name: "Jan"}, {name: "Feb"}, {name: "Mar"}, {name: "Apr"}, {name: "May"}, {name: "June"}, {name: "July"}, {name: "Aug"}, {name: "Sep"}, {name: "Oct"}, {name: "Nov"}, {name: "Dec"}];


function init(){
	
	username = parseParameters();
	
	if(username == "") {
		setInterval(displayStats,300, "");
	} else {
		prepare();
		queryService("https://bitbucket.org/api/2.0/repositories/" + username + "/");
	}
}

function parseParameters(){
	
	var username = "";
	var shareHTML = "<button class='button-primary'>Facebook</button> <button class='button-primary'>Twitter</button>";
	
	$( "#baseForm" ).submit(function( event ) {
		window.location.href = '?user=' + $('#id_username').val();
		event.preventDefault();
	});
	
	if(window.location.search == "" || window.location.search.length < 7) {
		$('#userInfo').html("<h6>Key in you're Bitbucket username to see publicly visible stats!</h6>");	
	} else {
		username = window.location.search.replace('?user=','')
		$("#userInfo").html("<h3>BitBucket :: " + username + "</h3>" + shareHTML);
	}
	
	return username;
}

function randomActivity(){
	var rand = Math.floor(Math.random()*(5-0+0)+0);
	return rand;
}

function displayStats(){
						
	var currentDate = new Date();
	var current_month = currentDate.getMonth();
	
	var statsDisplay = "";
	
	for(var i = current_month + 1; i < yearStats.length; i++)
	{
		var normalizedMonthCount = 0;
		if(mostMonthCommits > 0) {
			normalizedMonthCount = Math.round(yearStats[i].count*5/mostMonthCommits);
			
			if(yearStats[i].count > 0 && normalizedMonthCount == 0)
				normalizedMonthCount = 1;
				
		} else {
			normalizedMonthCount = randomActivity();
		}
	
		statsDisplay += '<div class="one column">';	
		statsDisplay += '<div class="twelve column activity-' + normalizedMonthCount.toString() + '"><b><i>' + yearStats[i].name + '</i></b></div>';	
		
		// Display days in a month. Accounts for 0 based index
		days = new Date(currentDate.getYear(), i + 1, 0).getDate();
		
		for(var k = 1; k < days + 1; k++)
		{
			var normalizedDayCount = 0;
			if(mostDayCommits > 0) {
				normalizedDayCount = Math.round(yearStats[i][k].count*5/mostDayCommits);
				
				if(yearStats[i][k].count > 0 && normalizedDayCount == 0)
					normalizedDayCount = 1;
				
			} else {
				normalizedDayCount = randomActivity();
			}
			
			statsDisplay += '<div class="twelve column activity-child activity-' + normalizedDayCount.toString() + '"><p class="small">' + k + '</p></div>';
		}
		
		statsDisplay += '</div>';
	
		if(i == current_month)
			break;
		
		if(i == yearStats.length - 1)
			i = -1;
	}
	
	$('#statsDisplay').html(statsDisplay);
	
}

function prepare(){
	
	var currentDate = new Date();
	var currentYear = currentDate.getYear();
	var currentMonth = currentDate.getMonth();
	
	var lastDate = new Date();
	lastDate.setMonth(currentDate.getMonth() - 12);
	var lastYearToLog = lastDate.getYear();
	var lastMonthToLog = lastDate.getMonth();
	
	// Initialize value for each day in the previous year
	for(var m = 0; m < yearStats.length; m++) {
		
		yearStats[m].count = 0;
		
		numDays = new Date(currentDate.getYear(), m + 1, 0).getDate();
		for(var n = 1; n <= numDays; n++) {
			
			yearStats[m][n] = {count: 0, details:[]};
			
		}
	}
}

function queryService(url){

	$.ajax({
		method: "GET",
		url: url,
		success: function(repoResult){
			
			if(repoResult.next != undefined)
				queryService(repoResult.next);
			
			// repoResult.values.length
			for(var i = 0; i < repoResult.values.length; i++){
				
				var repo = repoResult.values[i];
				var repoName = repo.name;
				var repoCommitAPI = repo.links.commits.href;

				$.ajax({
					method: "GET",
					url: repoCommitAPI,
					success: function(commitResult){
						
						for(var j = 0; j < commitResult.values.length; j++){
							
							var commit = commitResult.values[j];
							var commitDate = new Date(Date.parse(commit.date));
							
							// Calculate max commits in a month
							var previousMonthCount = yearStats[commitDate.getMonth()].count;
							if ((previousMonthCount + 1) > mostMonthCommits)
								mostMonthCommits = (previousMonthCount + 1);
							
							// Calculate max commits in a month
							var previousDayCount = yearStats[commitDate.getMonth()][commitDate.getDate()].count;
							if ((previousDayCount + 1) > mostDayCommits)
								mostDayCommits = (previousDayCount + 1);
							
							yearStats[commitDate.getMonth()].count = previousMonthCount + 1;
						
							yearStats[commitDate.getMonth()][commitDate.getDate()].count = previousDayCount + 1;
							yearStats[commitDate.getMonth()][commitDate.getDate()].details.push({repoName: repoName, commitDate: commit.date, commitURL: commit.links.html.href});
						}
						
						displayStats();
				}});
				
			}
	}});
	
}